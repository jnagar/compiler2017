GROUP 7: 12325, 12390
Milestone 1: Scanner and Parser
---------------------------------------------------------------------------------------------------------------------------------------------------
To execute :
make clean
make

#If you want to test on other test file then change the <file_name> in "Makefile" at line number 8.
#For example: python bin/parser test/<file_name.R>

About:
This is Scanner and Parser for R-language implemented in python using PLY (PYTHON LEX YACC).
 
Supports the following:

Operators: PLUS, MINUS, MULTIPLICATION, DIVISION, REMAINDER, QUOTIENT, EXPONENT, LOGICAL OR, LOGICAL AND, NOT, BITWISE OR, BITWISE AND, GREATER THAN, GREATER THAN EQUAL TO, LESS THAN, LESS THAN EQUAL TO, ASSIGNMENT, NOT EQUAL TO, EQUAL COMPARISION, SEARCH

Separators: Comma, Colon, DQuote , SQuote, Semicolon

Brackets: '(', ')','{','}','[',']'

Keywords: if, else, while, break, repeat, return, for, switch, next

Data types: NUMERIC, INTEGER, STRING, BOOLEAN (supports TRUE, FALSE but not T,F)

Ignores: Whitespaces, tabs, newline, Comments 

Identifier: Variable names in R, function names in R

Error: Undeclared Symbols or Undeclared datatypes produce "Illegal Character" error message. 

Output:
Generates a parse tree of given source code




