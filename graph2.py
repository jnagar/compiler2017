my_dict = {
	'start' : '1',
	'loop_statements' : '1',
	'conditional_statements' : '1',
	'expression_statements' : '1',
	'functions_statements' : '1',
	'function_definition' : '1',
	'arg_list' : '1',
	'argument' : '1',
	'arg' : '1',
	'function_call' : '1',
	'arg_to_pass' : '1',
	'if_else_statement' : '1',
	'if_statement' : '1',
	'compound_statement' : '1',
	'statement_list' : '1',
	'statement' : '1',
	'expression' : '1',
	'math' : '1',
	'jump_statements' : '1',
	'rightside' : '1',
	'if_cond' : '1',
	'comparison_statement' : '1',
	'compop' : '1',
	'factor' : '1',
	'logop' : '1',
	'bitop' : '1',
	'while_loop' : '1',
	'repeat_loop' : '1',
	'for_loop' : '1',
	'any_type' : '1',
	'vector' : '1'
}

f = open("graph2.dot",'w+')
f.write("digraph g1{\n")
for line in (open("graph1.dot").readlines()):
	words=line.split()
	if words[0] == 'digraph' or words[0] == '}':
		continue
	else:
		if words[2] == ';':
			f.write(words[0] + str(my_dict[words[0]]) + ' -> ' + 'None' + '\n')
		else:
			w1 = words[2]
			if w1 in my_dict:
				my_dict[words[2]] = str(int(my_dict[words[2]]) + 1)
				f.write(words[0] + str(my_dict[words[0]]) + ' -> ' + words[2] + str(my_dict[words[2]]) + '\n')
			else:
				f.write(words[0] + str(my_dict[words[0]]) + ' -> ' + words[2] + '\n')
f.write("}")		
f.close()
