f = open("myfile2.txt", 'w+')
for line in reversed(open("myfile.txt").readlines()):
   	f.write(line.rstrip() + "\n")
f.close()

f = open("graph1.dot",'w+')
f.write("digraph g1{\n")
for line in (open("myfile2.txt").readlines()):
	linelist = line.split()
	lenline = len(linelist)
	#print lenline
	if lenline == 3 and linelist[2] == ';':
		f.write(linelist[0] +" -> " + " None" + "\n")
	else:
		for i in xrange (2,lenline):
		#print i
			f.write(linelist[0] +" -> " + linelist[i] + "\n")

f.write("}")
f.close()
