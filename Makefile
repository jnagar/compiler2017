.PHONY: clean
all:
	chmod +x src/Lexer.py
	chmod +x src/Parser.py
	cp src/Lexer.py bin
	cp src/Parser.py bin
	mv bin/Parser.py bin/parser
	python bin/parser test/if_test.R
	python graph.py
	dot -Tpdf graph.dot -o graph.pdf
clean:
	rm bin/Lexer.py
	rm bin/parser
	rm bin/Lexer.pyc
	rm bin/parser.out
	rm bin/parsetab.py
	rm bin/parsetab.pyc
